Rails.application.routes.draw do

  root 'folders#index'
  devise_for :users

  resources :users
  resources :folders do
    member do
      get  'new_file_copy'
      post 'create_file_copy'
      post 'destroy_file'
    end
  end
  resources :security_levels
  resources :permissions
  resources :roles
  resources :role_permissions
  resources :user_roles

end
