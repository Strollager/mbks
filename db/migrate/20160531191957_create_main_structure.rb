class CreateMainStructure < ActiveRecord::Migration
  def change

    create_table :security_levels do |t|
      t.integer :lvl, null: false
    end

    create_table :folders do |t|
      t.string :path, null: false
      t.belongs_to :security_level, null: false
    end

    create_table :roles do |t|
      t.string :name, null: false
    end

    create_table :permissions do |t|
      t.belongs_to :security_level, null: false
      t.string  :name, null: false
      t.boolean :show,   default: false
      t.boolean :add,    default: false
      t.boolean :copy,   default: false
      t.boolean :remove, default: false
    end

    create_table :permissions_roles, :id => false do |t|
      t.belongs_to :role, null: false
      t.belongs_to :permission, null: false
    end

    create_table :roles_users, :id => false do |t|
      t.belongs_to :user, null: false
      t.belongs_to :role, null: false
    end

  end
end
