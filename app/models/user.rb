class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :roles

  def can_show?(lvl)
    bool = false

    self.roles.each do |role|
      role.permissions.each do |permission|
        if permission.security_level.lvl >= lvl
          bool = bool || permission.show
        elsif !bool
          bool = false
        end
      end
    end

    bool
  end

  def can_copy?(lvl)
    bool = false

    self.roles.each do |role|
      role.permissions.each do |permission|
        if permission.security_level.lvl >= lvl
          bool = bool || permission.copy
        elsif !bool
          bool = false
        end
      end
    end

    bool
  end

  def can_remove?(lvl)
    bool = false

    self.roles.each do |role|
      role.permissions.each do |permission|
        if permission.security_level.lvl >= lvl
          bool = bool || permission.remove
        elsif !bool
          bool = false
        end
      end
    end

    bool
  end



end
