class Permission < ActiveRecord::Base

  belongs_to :security_level

  validates :name, presence: true

end
