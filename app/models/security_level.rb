class SecurityLevel < ActiveRecord::Base
  validates :lvl, presence: true, numericality: true
end
