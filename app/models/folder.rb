class Folder < ActiveRecord::Base

  belongs_to :security_level

  validates :path, presence: true
  validates :security_level_id, presence: true

  def self.list(id, user)
    folders = Array.new

    Folder.all.where.not(id: id).each do |folder|
      if user.can_show?(folder.security_level.lvl)
        folders << folder
      end
    end

    folders
  end

  def self.secure_all(user)
    folders = Array.new

    Folder.all.each do |folder|
      if user.can_show?(folder.security_level.lvl)
        folders << folder
      end
    end

    folders
  end

end
