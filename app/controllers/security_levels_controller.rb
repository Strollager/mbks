class SecurityLevelsController < ApplicationController

  def index
    @security_levels = SecurityLevel.all
  end

  def show
    @security_level = SecurityLevel.find_by_id params[:id]
  end

  def new
    @security_level = SecurityLevel.new
  end

  def create
    @security_level = SecurityLevel.new(security_level_params)
    if @security_level.save
      redirect_to security_levels_path
    end
  end

  def edit
    @security_level = SecurityLevel.find_by_id params[:id]
  end

  def update
    @security_level = SecurityLevel.find_by_id params[:id]
    if @security_level.update_attributes(security_level_params)
      redirect_to security_levels_path
    end
  end

  def destroy
    @security_level = SecurityLevel.find_by_id(params[:id]).destroy
    redirect_to security_levels_path
  end

  private

    def security_level_params
      params.require(:security_level).permit!
    end

end
