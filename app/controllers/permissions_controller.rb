class PermissionsController < ApplicationController

  def index
    @permissions = Permission.all
  end

  def show
    @permission = Permission.find_by_id params[:id]
  end

  def new
    @permission = Permission.new
  end

  def create
    @permission = Permission.new(permission_params)
    if @permission.save
      redirect_to permissions_path, notice: 'Разрешение успешно создано.'
    end
  end

  def edit
    @permission = Permission.find_by_id params[:id]
  end

  def update
    @permission = Permission.find_by_id params[:id]
    if @permission.update_attributes(permission_params)
      redirect_to permissions_path, notice: 'Разрешение успешно обновлено.'
    end
  end

  def destroy
    @permission = Permission.find_by_id(params[:id]).destroy
    redirect_to permissions_path, notice: 'Разрешение успешно удалено.'
  end

  private

    def permission_params
      params.require(:permission).permit!
    end

end
