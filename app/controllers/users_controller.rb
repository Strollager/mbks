class UsersController < ApplicationController

  def index
    @users = User.all
  end

  def show
    @user = User.find_by_id params[:id]
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find_by_id params[:id]
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to users_path, notice: 'Пользователь успешно создан.'
    end
  end

  def update
    @user = User.find_by_id params[:id]
    if @user.update_attributes(user_params)
      redirect_to users_path, notice: 'Пользователь успешно обновлен.'
    end
  end

  def destroy
    @user = User.find_by_id(params[:id]).destroy
    redirect_to users_path, notice: 'Пользователь успешно удален.'
  end

  private

    def user_params
      params.require(:user).permit!
    end

end
