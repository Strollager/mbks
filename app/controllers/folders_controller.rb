class FoldersController < ApplicationController

  def index
    @folders = Folder.secure_all(current_user)
  end

  def show
    @folder = Folder.find_by_id params[:id]
    @files = Dir.glob(@folder.path + '/**/*')
  end

  def new
    @folder = Folder.new
  end

  def create
    @folder = Folder.new(folder_params)
    if @folder.save
      redirect_to folders_path, notice: 'Директория успешно создана.'
    end
  end

  def edit
    @folder = Folder.find_by_id params[:id]
  end

  def update
    @folder = Folder.find_by_id params[:id]
    if @folder.update_attributes(folder_params)
      redirect_to folders_path, notice: 'Директория успешно обновлена.'
    end
  end

  def destroy
    @folder = Folder.find_by_id(params[:id]).destroy
    redirect_to folders_path, notice: 'Директория успешно удалена.'
  end

  # custom actions

  def new_file_copy
    @current_folder = Folder.find_by_id params[:id]
  end

  def create_file_copy
    old_folder = Folder.find_by_id params[:old_folder_id]
    new_folder = Folder.find_by_id params[:new_folder_id]
    file = params[:file_path]

    # если пытаемся скопировать из более высокого уровня в более низкий
    return (redirect_to folders_path, alert: 'Доступ запрещен.') if old_folder.security_level.lvl > new_folder.security_level.lvl
    return (redirect_to folders_path, alert: 'Доступ запрещен.') unless current_user.can_copy?(new_folder.security_level.lvl)

    FileUtils.cp(file, new_folder.path)
    redirect_to folders_path, notice: 'Файл успешно скопирован.'
  end

  def destroy_file
    File.delete(params[:file_path])
    redirect_to folder_path(params[:current_folder_id]), notice: 'Файл успешно удален.'
  end

  private

    def folder_params
      params.require(:folder).permit!
    end

end
