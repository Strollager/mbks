class RolesController < ApplicationController

  def index
    @roles = Role.all
  end

  def show
    @role = Role.find_by_id params[:id]
  end

  def new
    @role = Role.new
  end

  def create
    @role = Role.new(role_params)
    if @role.save
      redirect_to roles_path, notice: 'Роль успешно создана.'
    end
  end

  def edit
    @role = Role.find_by_id params[:id]
  end

  def update
    @role = Role.find_by_id params[:id]
    if @role.update_attributes(role_params)
      redirect_to roles_path, notice: 'Роль успешно обновлена.'
    end
  end

  def destroy
    @role = Role.find_by_id(params[:id]).destroy
    redirect_to roles_path, notice: 'Роль успешно удалена.'
  end

  private

    def role_params
      params.require(:role).permit!
    end

end
